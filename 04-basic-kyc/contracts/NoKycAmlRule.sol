pragma solidity ^0.4.18;


import 'KycAmlRule.sol';


contract NoKycAmlRule is KycAmlRule {
    function canDeposit(address /*_asset*/, address /*_to*/, uint /*_value*/)
    public
    view
    returns (bool) {
        return true;
    }

    function canTransfer(address /*_asset*/, address /*_from*/, address /*_to*/, uint /*_value*/)
    public
    view
    returns (bool) {
        return true;
    }

    function canWithdraw(address /*_asset*/, address /*_from*/, uint /*_value*/)
    public
    view
    returns (bool) {
        return true;
    }
}
