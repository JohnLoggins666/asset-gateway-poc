pragma solidity ^0.4.18;


import 'KycAmlRule.sol';
import 'KycAmlToken.sol';


contract FullKycAmlRule is KycAmlRule {

    function canDeposit(address _asset, address _to, uint /*_value*/)
    public
    view
    returns (bool) {
        KycAmlToken kycAmlERC20 = KycAmlToken(_asset);
        bool result = (kycAmlERC20.kycVerified(_to) == true);
        return result;
    }

    function canTransfer(address _asset, address _from, address _to, uint /*_value*/)
    public
    view
    returns (bool) {
        KycAmlToken kycAmlERC20 = KycAmlToken(_asset);
        bool result = (kycAmlERC20.isKycVerified(_from) == true) && (kycAmlERC20.kycVerified(_to) == true);
        return result;
    }

    function canWithdraw(address _asset, address _from, uint /*_value*/)
    public
    view
    returns (bool) {
        KycAmlToken kycAmlERC20 = KycAmlToken(_asset);
        bool result = (kycAmlERC20.isKycVerified(_from) == true);
        return result;
    }
}
