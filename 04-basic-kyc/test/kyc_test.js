const {
    expect,
    expectNoAsyncThrow,
    expectThrow,
    solc,
    ganacheWeb3,
} = require('chain-dsl/test/helpers')
const {send, call} = require('chain-dsl')
const deployer = require('../lib/deployer')

const mint = 'mint(address,uint256)'
const mintForSelf = 'mint(uint256)'
const withdraw = 'withdraw'
const burnFrom = 'burnFrom'
const deposit = 'deposit'
const approve = 'approve'
const transfer = 'transfer'
const transferFrom = 'transferFrom'
const setKycVerified = 'setKycVerified'
const kycVerified = 'kycVerified'

describe("Asset Gateway", function () {
    this.timeout(5000)

    let web3, snaps, accounts,
        gate, boundaryKycGate, fullKycGate,
        DEPLOYER,
        OPERATOR,
        CUSTOMER,
        CUSTOMER1,
        CUSTOMER2,
        AMT

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER,
            CUSTOMER1,
            CUSTOMER2
        ] = accounts = await web3.eth.getAccounts()

        AMT = 100

        ;({
            gate,
            boundaryKycGate,
            fullKycGate
        } = await deployer.base(web3, solc(__dirname, '../solc-input.json'), DEPLOYER, OPERATOR))

        await send(boundaryKycGate, OPERATOR, setKycVerified, OPERATOR, true)
        await send(fullKycGate, OPERATOR, setKycVerified, OPERATOR, true)
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    it("operators can update others' KYC status", async () => {
        await send(gate, OPERATOR, setKycVerified, CUSTOMER, true)
        expect(await call(gate, kycVerified, CUSTOMER)).eql(true)

        await send(gate, OPERATOR, setKycVerified, CUSTOMER, false)
        expect(await call(gate, kycVerified, CUSTOMER)).eql(false)
    })

    it("operators can update their own KYC status", async () => {
        await send(gate, OPERATOR, setKycVerified, OPERATOR, true)
        expect(await call(gate, kycVerified, OPERATOR)).eql(true)

        await send(gate, OPERATOR, setKycVerified, OPERATOR, false)
        expect(await call(gate, kycVerified, OPERATOR)).eql(false)
    })

    it("non-operators can NOT update anyone's KYC status", async () => {
        await expectThrow(async () =>
            send(gate, DEPLOYER, setKycVerified, CUSTOMER, true))

        await expectThrow(async () =>
            send(boundaryKycGate, DEPLOYER, setKycVerified, CUSTOMER, true))

        await expectThrow(async () =>
            send(fullKycGate, DEPLOYER, setKycVerified, CUSTOMER, true))
    })

    describe("with no KYC constraints", async () => {

        it("allows deposits FROM any address", async () => {
            await expectNoAsyncThrow(async () => {
                await send(gate, CUSTOMER, deposit, AMT)
                await send(gate, OPERATOR, mint, CUSTOMER, AMT)
            })
        })

        it("allows transfers TO any address", async () => {
            await send(gate, CUSTOMER1, deposit, AMT)
            await send(gate, OPERATOR, mint, CUSTOMER1, AMT)

            await expectNoAsyncThrow(async () => {
                await send(gate, CUSTOMER1, transfer, CUSTOMER2, AMT)
            })
        })

        it("allows widthdrawal FROM any address", async () => {
            await send(gate, CUSTOMER1, deposit, AMT)
            await send(gate, OPERATOR, mint, CUSTOMER1, AMT)

            await expectNoAsyncThrow(async () => {
                await send(gate, CUSTOMER1, withdraw, AMT)
                await send(gate, CUSTOMER1, transfer, OPERATOR, AMT)
                await send(gate, OPERATOR, burnFrom, CUSTOMER1, AMT)
            })
        })
    })

    describe("with boundary KYC", async () => {

        describe("OPERATOR", async () => {

            it("can update customers' KYC verified status", async () => {
                await send(boundaryKycGate, OPERATOR, setKycVerified, CUSTOMER, true)
                expect(await call(boundaryKycGate, kycVerified, CUSTOMER)).eql(true)

                await send(boundaryKycGate, OPERATOR, setKycVerified, CUSTOMER, false)
                expect(await call(boundaryKycGate, kycVerified, CUSTOMER)).eql(false)
            })
        })

        describe("CUSTOMER", async () => {

            it("allows deposits TO KYC verified addresses", async () => {
                await send(boundaryKycGate, OPERATOR, setKycVerified, CUSTOMER, true)

                await expectNoAsyncThrow(async () => {
                    await send(boundaryKycGate, CUSTOMER, deposit, AMT)
                    await send(boundaryKycGate, OPERATOR, mint, CUSTOMER, AMT)
                })
            })

            it("rejects deposits FROM non-KYC verified addresses", async () => {
                await expectThrow(async () =>
                    send(boundaryKycGate, CUSTOMER, deposit, AMT))
            })

            it("allows transfers FROM KYC verified address to arbitrary addresses", async () => {
                await send(boundaryKycGate, OPERATOR, setKycVerified, CUSTOMER1, true)
                await send(boundaryKycGate, CUSTOMER1, deposit, AMT)
                await send(boundaryKycGate, OPERATOR, mint, CUSTOMER1, AMT)

                await expectNoAsyncThrow(async () =>
                    send(boundaryKycGate, CUSTOMER1, transfer, CUSTOMER2, AMT))
            })

            it("allows transfers FROM non-KYC verified addresses", async () => {
                await send(boundaryKycGate, OPERATOR, setKycVerified, CUSTOMER1, true)
                await send(boundaryKycGate, CUSTOMER1, deposit, AMT)
                await send(boundaryKycGate, OPERATOR, mint, CUSTOMER1, AMT)
                await send(boundaryKycGate, OPERATOR, setKycVerified, CUSTOMER1, false)

                await expectNoAsyncThrow(async () =>
                    send(boundaryKycGate, CUSTOMER1, transfer, CUSTOMER2, AMT))
            })

            it("allows withdrawal FROM any KYC verified address", async () => {
                await send(boundaryKycGate, OPERATOR, setKycVerified, CUSTOMER1, true)
                await send(boundaryKycGate, CUSTOMER1, deposit, AMT)
                await send(boundaryKycGate, OPERATOR, mint, CUSTOMER1, AMT)

                await expectNoAsyncThrow(async () => {
                    await send(boundaryKycGate, CUSTOMER1, withdraw, AMT)
                    await send(boundaryKycGate, CUSTOMER1, transfer, OPERATOR, AMT)
                    await send(boundaryKycGate, OPERATOR, burnFrom, CUSTOMER1, AMT)
                })
            })

            it("rejects withdrawal FROM non-KYC verified addresses", async () => {
                await send(boundaryKycGate, OPERATOR, setKycVerified, CUSTOMER1, true)
                await send(boundaryKycGate, CUSTOMER1, deposit, AMT)
                await send(boundaryKycGate, OPERATOR, mint, CUSTOMER1, AMT)
                await send(boundaryKycGate, OPERATOR, setKycVerified, CUSTOMER1, false)

                await expectThrow(async () => {
                    await send(boundaryKycGate, CUSTOMER1, withdraw, AMT)
                    await send(boundaryKycGate, CUSTOMER1, transfer, OPERATOR, AMT)
                    await send(boundaryKycGate, OPERATOR, burnFrom, CUSTOMER1, AMT)
                })
            })
        })
    })

    describe("with full KYC", async () => {

        describe("OPERATOR", async () => {

            it("can update customers' KYC verified status", async () => {
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER, true)
                expect(await call(fullKycGate, kycVerified, CUSTOMER)).eql(true)

                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER, false)
                expect(await call(fullKycGate, kycVerified, CUSTOMER)).eql(false)
            })
        })

        describe("CUSTOMER", async () => {

            it("allows deposits TO KYC verified addresses", async () => {
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER, true)

                await expectNoAsyncThrow(async () => {
                    await send(fullKycGate, CUSTOMER, deposit, AMT)
                    await send(fullKycGate, OPERATOR, mint, CUSTOMER, AMT)
                })
            })

            it("rejects deposits FROM non-KYC verified addresses", async () => {
                await expectThrow(async () =>
                    send(fullKycGate, CUSTOMER, deposit, AMT))
            })

            it("allows transfers BETWEEN KYC verified addresses", async () => {
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER1, true)
                await send(fullKycGate, CUSTOMER1, deposit, AMT)
                await send(fullKycGate, OPERATOR, mint, CUSTOMER1, AMT)

                // Difference from boundary KYC
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER2, true)

                await expectNoAsyncThrow(async () =>
                    send(fullKycGate, CUSTOMER1, transfer, CUSTOMER2, AMT))
            })

            it("reject transfers FROM non-KYC verified addresses", async () => {
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER1, true)
                await send(fullKycGate, CUSTOMER1, deposit, AMT)
                await send(fullKycGate, OPERATOR, mint, CUSTOMER1, AMT)
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER1, false)

                // Difference from boundary KYC
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER2, true)

                await expectThrow(async () =>
                    send(fullKycGate, CUSTOMER1, transfer, CUSTOMER2, AMT))
            })

            // Extra case compared to boundary KYC
            it("reject transfers TO non-KYC verified addresses", async () => {
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER1, true)
                await send(fullKycGate, CUSTOMER1, deposit, AMT)
                await send(fullKycGate, OPERATOR, mint, CUSTOMER1, AMT)

                await expectThrow(async () =>
                    send(fullKycGate, CUSTOMER1, transfer, CUSTOMER2, AMT))
            })

            it("allows withdrawal FROM any KYC verified address", async () => {
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER1, true)
                await send(fullKycGate, CUSTOMER1, deposit, AMT)
                await send(fullKycGate, OPERATOR, mint, CUSTOMER1, AMT)

                await expectNoAsyncThrow(async () => {
                    await send(fullKycGate, CUSTOMER1, withdraw, AMT)
                    await send(fullKycGate, CUSTOMER1, transfer, OPERATOR, AMT)
                    await send(fullKycGate, OPERATOR, burnFrom, CUSTOMER1, AMT)
                })
            })

            it("rejects withdrawal FROM non-KYC verified addresses", async () => {
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER1, true)
                await send(fullKycGate, CUSTOMER1, deposit, AMT)
                await send(fullKycGate, OPERATOR, mint, CUSTOMER1, AMT)
                await send(fullKycGate, OPERATOR, setKycVerified, CUSTOMER1, false)

                await expectThrow(async () => {
                    await send(fullKycGate, CUSTOMER1, withdraw, AMT)
                    await send(fullKycGate, CUSTOMER1, transfer, OPERATOR, AMT)
                    await send(fullKycGate, OPERATOR, burnFrom, CUSTOMER1, AMT)
                })
            })
        })
    })
})
