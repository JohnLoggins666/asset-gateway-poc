# 04. _Basic KYC_ phase — Smart contracts

## Setup

```bash
pnpm install
```

## Test

Run the test suite continuously to follow both test source code and
smart contract code changes:

```bash
pnpm test --watch
```
