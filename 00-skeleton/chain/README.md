# Ethereum blockchain module

## Setup

```bash
npm install -g pnpm
npm init --yes
pnpm install --save-dev 'git+https://git@github.com/enumatech/ganache-core#explicit-abstract-leveldown-dep'
pnpm i -D mocha
pnpx mocha --watch
# or
pnpm test
```
