with import (builtins.fetchTarball "https://d3g5gsiof5omrk.cloudfront.net/nixos/unstable/nixos-18.03pre121706.b9347167822/nixexprs.tar.xz") {};

stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };

  buildInputs = [
    nodejs-8_x
  ];
}
