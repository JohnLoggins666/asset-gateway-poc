import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KycprocessComponent } from './kycprocess.component';

describe('VerifyRequestComponent', () => {
  let component: KycprocessComponent;
  let fixture: ComponentFixture<KycprocessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KycprocessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KycprocessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
