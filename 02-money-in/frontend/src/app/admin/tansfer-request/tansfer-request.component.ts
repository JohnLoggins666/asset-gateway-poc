import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-tansfer-request',
  templateUrl: './tansfer-request.component.html',
  styleUrls: ['./tansfer-request.component.scss']
})
export class TansferRequestComponent implements OnInit {

  verifyStatus = 0;
  toggle = false;

  actions= [{
    name: 'Mint',
  }, {
    name: 'Redeeption',
  }, {
    name: 'Amount',
  }];

  rows = [{
    no: '001',
    type: 'Mint',
    userId: '0891231883839',
    ccy: 'USD',
    amount: '127287383.45',
    date: '10 Oct 2017',
    status: 1,
    action: 1,
  }];

  constructor(private location: Location) { }

  ngOnInit() {
  }

  verify() {
    setTimeout(() => {
      this.verifyStatus = 1;
    }, 2000);
  }

  goBack(): void {
    this.location.back();
  }

  chage(): void {
    this.toggle = true;
  }
}
