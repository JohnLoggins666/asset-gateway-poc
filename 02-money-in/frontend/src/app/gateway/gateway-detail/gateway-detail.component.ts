import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DataSource} from '@angular/cdk/collections';

export interface Element {
  token: string;
  price: string;
  volume: any;
  change: any;
  name: string;
}

const dataSource: Element[] = [
  {token: 'USD1', price: '0.01', volume: 1.0079, change: '+ 10%', name: 'Ethereum'},
  {token: 'USD2', price: '0.11', volume: 4.0026, change: '- 11%', name: 'Ethereum'},
  {token: 'USD3', price: '12.01', volume: 6.941, change: '+ 5%', name: 'Ethereum'},
  {token: 'USD4', price: '3.01', volume: 9.0122, change: '+ 16%', name: 'Ethereum'},
  {token: 'USD5', price: '2.31', volume: 10.811, change: '- 9%', name: 'Ethereum'},
  {token: 'USD5', price: '0.51', volume: 12.0107, change: '+ 100%', name: 'Ethereum'},
  {token: 'US6D', price: 'Nitrogen', volume: 14.0067, change: '+ 10%', name: 'Ethereum'},
  {token: 'USD7', price: 'Oxygen', volume: 15.9994, change: 'O', name: 'Ethereum'},
  {token: 'USD', price: 'Fluorine', volume: 18.9984, change: 'F', name: 'Ethereum'},
  {token: 'USD', price: 'Neon', volume: 20.1797, change: 'Ne', name: 'Ethereum'},
  {token: 'USD', price: 'Sodium', volume: 22.9897, change: 'Na', name: 'Ethereum'},
  {token: 'USD', price: 'Magnesium', volume: 24.305, change: 'Mg', name: 'Ethereum'},
  {token: 'USD', price: 'Aluminum', volume: 26.9815, change: 'Al', name: 'Ethereum'},
  {token: 'USD', price: 'Silicon', volume: 28.0855, change: 'Si', name: 'Ethereum'},
  {token: 'USD', price: 'Phosphorus', volume: 30.9738, change: 'P', name: 'Ethereum'},
  {token: 'USD', price: 'Sulfur', volume: 32.065, change: 'S', name: 'Ethereum'},
  {token: 'USD', price: 'Chlorine', volume: 35.453, change: 'Cl', name: 'Ethereum'},
  {token: 'USD', price: 'Argon', volume: 39.948, change: 'Ar', name: 'Ethereum'},
  {token: 'USD', price: 'Potassium', volume: 39.0983, change: 'K', name: 'Ethereum'},
  {token: 'USD', price: 'Calcium', volume: 40.078, change: 'Ca', name: 'Ethereum'},
];

/**
 * Data source to provide what data should be rendered in the table. The observable provided
 * in connect should emit exactly the data that should be rendered by the table. If the data is
 * altered, the observable should emit that new set of data on the stream. In our case here,
 * we return a stream that contains only one set of data that doesn't change.
 */
export class ExampleDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Element[]> {
    return Observable.of(dataSource);
  }

  disconnect() {}
}

@Component({
  selector: 'app-gateway-detail',
  templateUrl: './gateway-detail.component.html',
  styleUrls: ['./gateway-detail.component.scss']
})
export class GatewayDetailComponent implements OnInit {

  displayedColumns = ['token', 'price', 'volume', 'change', 'name'];
  dataSource = new ExampleDataSource();

  constructor() { }

  ngOnInit() {
  }



}
