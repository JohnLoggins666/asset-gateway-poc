import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-gateway-kycprocess',
  templateUrl: './gateway-kycprocess.component.html',
  styleUrls: ['./gateway-kycprocess.component.scss']
})
export class GatewayKycprocessComponent implements OnInit {
  file: Array<Object>;
  constructor( private location: Location
  ) {
  }

  ngOnInit() {
  }

  goBack() {
    this.location.back();
  }

  imageUploaded(event) {
    console.log(event);
    this.file.push(event.file);
    console.log(this.file);
  }
  imageRemoved(event) {
    console.log(event);
    const index = this.file.indexOf(event.file);
    if ( index > -1) {
      this.file.splice(index, 1);
    }
    console.log(this.file);
  }
}
