import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material';
import {Web3Service} from "../../util/web3.service";
import {Router} from '@angular/router';

@Component({
  // selector: 'app-gateway-mint',
  selector: 'app-gateway-withdraw-transfer',
  templateUrl: './gateway-withdraw-transfer.component.html',
  styleUrls: ['./gateway-withdraw-transfer.component.scss']
})
export class GatewayWithdrawTransferComponent implements OnInit {

  layouts = [
    {text: 'One', cols: 4, rows: 4, color: 'lightblue'},
    {text: 'Two', cols: 12, rows: 12, color: 'lightgreen'},
    {text: 'Three', cols: 4, rows: 8, color: 'lightpink'},
  ];

  tokenLists = [
    {img: '', name: 'AcmeUSD', amount: 1000},
    {img: '', name: 'Acme HKD', amount: 1000},
  ]



  model = {
    amount: 0,
  };

  assetGatewayAddress = '0xf17f52151ebef6c7334fad080c5704d77216b732';

  constructor(
    private authService: AuthService,
    private _web3Service: Web3Service,
    private  location: Location,
    private _router: Router,
    public snackBar: MatSnackBar
  ) {
  }


  ngOnInit() {

  }

  goBack() {
    this.location.back();
  }

  async submit() {

    if (this.model.amount <= 0) {
      this.snackBar.open("Amount is negative", "Okay", {
        duration: 2000,
      });
      return;
    }
    await this._web3Service.transfer(this.assetGatewayAddress,this.model.amount)
    this._router.navigate(["/en-US/gateway/dashboard/withdraw/mint"])

  }

}
