import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import * as _ from 'lodash';
import {of} from 'rxjs/observable/of';
import {catchError, tap} from 'rxjs/operators';
import * as uuid from 'uuid'


export interface WithdrawRequest {

  id: number;
  ccy: string;
  amount: number;
  userId: string,
  state: string
  address: string,
  timestamp: number,

  //transfer id to represent current transaction
  transferId:string,

}


@Injectable()
export class WithdrawService {

  public requests: WithdrawRequest[];

  constructor(private _http: HttpClient) {
  }

  allRequest(): Observable<WithdrawRequest[]> {

    return this._http.get('http://localhost:5000/api/v1/withdrawal-requests')

      .map((res: any) => {

        return _.map(res.data, (value, key) => {

          return {
            id: key,
            ccy: 'USD',
            amount: value.amount,
            userId: value.ethereumAddress,
            state: value.status,
            address: value.ethereumAddress,
            timestamp: value.timestamp,

            transferId: uuid.v4(),

          }
        })

      })
      .pipe(
        tap(x => this.requests = x),
        catchError(this.handleError('getHeroes', []))
      );
  }


  requestById(id: Observable<string>): Observable<WithdrawRequest> {

    return id.flatMap(id => {
      let r = _.find(this.requests, x => `${x.id}` === id)
      return of(r)
    })
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
