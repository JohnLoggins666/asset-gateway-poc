import { Component, OnInit } from '@angular/core';
import {Web3Service} from "../../util/web3.service";
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-register-done',
  templateUrl: './register-done.component.html',
  styleUrls: ['./register-done.component.scss']
})
export class RegisterDoneComponent implements OnInit {

  model = {
    email: '',
    password:'',
    account: ''
  };

  username =  '';
  ethereumAddress =  '';

  constructor(private web3Service: Web3Service, private _aRouter: ActivatedRoute) {

    //monitor if there are accounts available
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.model.account = accounts[0];
    });

  }
  ngOnInit() {
    this._aRouter.queryParamMap
      .subscribe(ps => {
        this.username = ps.get('username');
        this.ethereumAddress = ps.get('ethereumAddress');
      });

  }

}
