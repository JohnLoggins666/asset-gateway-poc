import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {OAuthService} from 'angular-oauth2-oidc';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  model = {
    username: '',
    password: '',
  };

  constructor(private oauthService: OAuthService, private _http: HttpClient, private _router: Router, private _translateService: TranslateService) {

  }

  ngOnInit() {
  }

  /**
   * Sign in
   */


  signin() {
    this.oauthService.initImplicitFlow()
  }

  public signout() {
    this.oauthService.logOut();
  }

}
