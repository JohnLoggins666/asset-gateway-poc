const createEventHandler = require('../services/blockchain-event-handler')

module.exports = (web3, gate) => {
    const {withdrawalRequests} = createEventHandler(web3, gate)

    return {
        getRequests: async (req, res, next) => {
            res.json({data: await withdrawalRequests()})
            next()
        }
    }
}
