const createEventHandler = require('../services/blockchain-event-handler')

module.exports = (web3, gate) => {
    const {depositRequests} = createEventHandler(web3, gate)

    return {
        async getRequests(req, res, next) {
            const deposits = await depositRequests()
            res.json({data: deposits})
            next()
        }
    }
}


