const {
    expect,
    toBN,
    solc,
    ganacheWeb3,
} = require('chain-dsl/test/helpers')
const deployer = require('../lib/deployer')

describe('Explorations', () => {
    let web3, accounts, snaps, accessControl, gate,
        DEPLOYER,
        OPERATOR,
        CUSTOMER

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER
        ] = accounts = await web3.eth.getAccounts()

        ;({
            gate,
            accessControl
        } = await deployer.base(web3, solc(__dirname, '../solc-input.json'), DEPLOYER, OPERATOR))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    it('Operator is authorized', async () => {
        const OPERATOR_ROLE = await accessControl.methods.OPERATOR().call()
        const OPERATOR_ROLE_MASK = 2 ** OPERATOR_ROLE

        expect(await accessControl.methods.getUserRoles(OPERATOR).call())
            .eq(OPERATOR_ROLE_MASK)

        const mintSig = web3.eth.abi.encodeFunctionSignature('mint(address,uint256)')
        expect(await accessControl.methods.getCapabilityRoles(gate.options.address, mintSig).call())
            .eq(OPERATOR_ROLE_MASK)
    })

    it('Can burn allowance', async () => {
        await gate.methods
            ['mint(address,uint256)'](CUSTOMER, toBN(10))
            .send({from: OPERATOR})

        await gate.methods
            ['approve(address,uint256)'](OPERATOR, toBN(5))
            .send({from: CUSTOMER})

        await gate.methods
            ['burn(address,uint256)'](CUSTOMER, toBN(3))
            .send({from: OPERATOR})

        expect(await gate.methods.allowance(CUSTOMER, OPERATOR).call()).eq(2)
        expect(await gate.methods.balanceOf(CUSTOMER).call()).eq(7)
    })
})
