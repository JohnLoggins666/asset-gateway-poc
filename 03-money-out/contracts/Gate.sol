pragma solidity 0.4.19;


import "dappsys.sol"; // auth, token
import "solovault.sol";
import "GateRoles.sol";
import "FiatToken.sol";


contract Gate is DSSoloVault, ERC20Events {
    event DepositRequested(address indexed by, uint256 amount);

    event WithdrawalRequested(address indexed from, uint256 amount);

    event Withdrawn(address indexed from, uint256 amount);

    event Log(uint8 x, bytes4 sig);

    function Gate(DSAuthority _authority, DSToken fiatToken)
    public
    {
        //        uint8 role = ac.OPERATOR();
        //        bytes4 method = bytes4(keccak256("mintFor(address,uint256)"));
        //        Log(role, method);
        //        ac.setRoleCapability(role, this, method, true);
        swap(fiatToken);
        setAuthority(_authority);
        setOwner(0x0);
    }

    function deposit(uint256 amount) public {
        DepositRequested(msg.sender, amount);
    }

    function mint(address guy, uint wad) public {
        super.mint(guy, wad);
        /* Because the EIP20 standard says so, we emit a Transfer event:
           A token contract which creates new tokens SHOULD trigger a
           Transfer event with the _from address set to 0x0 when tokens are created.
            (https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md)
        */
        Transfer(0x0, this, wad);
    }

    function withdraw(uint256 amount) public {
        WithdrawalRequested(msg.sender, amount);
    }

    function burn(address guy, uint wad) public {
        super.burn(guy, wad);
        Withdrawn(guy, wad);
    }
}
