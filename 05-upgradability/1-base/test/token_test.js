const {
    expect,
    expectAsyncThrow,
    expectNoAsyncThrow,
    expectThrow,
    solc,
    ganacheWeb3,
} = require('chain-dsl/test/helpers')

const deploy = require('../lib/deployer')
const {send} = require('chain-dsl')

describe('Token', function () {
    let web3, snaps, accounts, DEPLOYER, OPERATOR, CUSTOMER, token
    const wad = 10

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            CUSTOMER
        ] = accounts = await web3.eth.getAccounts()

        ;({token} = await deploy.base(web3, solc(__dirname, '../solc-input.json'), DEPLOYER, OPERATOR))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    it("allows transfer", async function () {
        await expectNoAsyncThrow(async () =>
            send(token, DEPLOYER, 'transfer', CUSTOMER, wad))
    })

})
