# 05. _Upgradability_ phase

Explore how to go from a KYC-less token which is deployed to production
to KYC aware token?

# Constraints and assumptions

1. An upgradeable system will have an external and internal users.
1. Internal users are for example the owners, operators and deployers of the contracts.
1. An upgrade should preserve the contract addresses used by external users, but can alter the ones for the internal users.
1. The ERC20 contract address stays the same over time after upgrades, all the logic change are reflected in another contract that is somehow linked to the ERC20 token contract.
    - 1.1 As a result, one of the solution si to have two contracts, 1) Owner contract 2) ERC20 contract pointed to the aforementioned Owner contract
    
## 0-goal

This folder establishes a complete functionality,
which should be reached after upgrade.


## 1-base

This folder serves a starting point for upgrades.


## 2-upgraded 

This folder demonstrates how the features in `0-goal` can delivered
in 2 steps via an upgrade.
