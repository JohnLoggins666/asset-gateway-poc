const {
    expect,
    expectAsyncThrow,
    expectNoAsyncThrow,
    expectThrow,
    solc,
    ganacheWeb3,
} = require('chain-dsl/test/helpers')
const {send} = require('chain-dsl')
const deployer = require('../lib/deployer')

describe('UpgradeAbility', function () {
    let web3, snaps, accounts, DEPLOYER, CUSTOMER, NOT_CUSTOMER, token, whitelistToken
    const wad = 10

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            CUSTOMER,
            NOT_CUSTOMER
        ] = accounts = await web3.eth.getAccounts()

        ;({token, whitelistToken} = await deployer.latest(web3, solc(__dirname, '../solc-input.json'), DEPLOYER))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    it("persists account balances over migration", async function () {
    })
})
