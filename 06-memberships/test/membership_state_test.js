const {
    expect,
    expectAsyncThrow,
    expectNoAsyncThrow,
    expectThrow,
    toBN,
    solc,
    ganacheWeb3,
} = require('chain-dsl/test/helpers')

const {
    address,
    wad,
    send,
    call,
    txEvents,
    sendEth
} = require('chain-dsl')

const deployer = require('../lib/deployer')

const addMembershipType = 'addMembershipType(uint8,uint8)'
const addMember = 'addMember(uint8)'
const addMemberFor = 'addMemberFor(uint8,address)'
const transferMember = 'transferMember(uint8,address)'
const transferMemberFor = 'transferMemberFor(uint8,address,address)'
const removeMember = 'removeMember(uint8)'
const removeMemberFor = 'removeMemberFor(uint8,address)'
const members = 'members(uint8,address)'
const approve = 'approve'
const lastLimitResetTime = 'lastLimitResetTime'
const PARTICIPANT_MEMBERSHIP_NAME = 1
const VOTING_MEMBERSHIP_NAME = 2
const NEW_MEMBERSHIP_NAME = 3

describe("OAX Foundation Membership State Contract", function () {
    let web3, snaps, accounts,
        oaxToken,
        membershipState,
        memberships,
        membershipLogicV2,
        DEPLOYER,
        OPERATOR,
        CUSTOMER,
        CUSTOMER_TWO,
        MIN_AMT,
        AMT,
        DEFAULT_DAILY_LIMIT,
        PARTICIPANT_MEMBERSHIP_FEE,
        VOTING_MEMBERSHIP_FEE

    before('deployment', async () => {
        this.timeout(6000)
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER,
            CUSTOMER_TWO
        ] = accounts = await web3.eth.getAccounts()

        MIN_AMT = wad(1)
        AMT = wad(100)
        DEFAULT_DAILY_LIMIT = wad(10000)
        PARTICIPANT_MEMBERSHIP_FEE = AMT
        VOTING_MEMBERSHIP_FEE = AMT * 2

        ;({
            oaxToken,
            membershipState,
            memberships,
            membershipLogicV2
        } = await deployer.base(web3, solc(__dirname, '../solc-input.json'),
            DEPLOYER,
            OPERATOR,
            OPERATOR,
            DEFAULT_DAILY_LIMIT))

        //set state contract owner to be logic contract
        await send(membershipState, DEPLOYER, "setOwner", memberships.options.address)
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    it("Only logic contract owner or super admin can trigger logic contract to change state contract owner", async () => {
        await expectAsyncThrow(async () => {
            await send(memberships, OPERATOR, "changeStateOwner", membershipLogicV2.options.address)
        })
        await expectNoAsyncThrow(async () => {
            await send(memberships, DEPLOYER, "changeStateOwner", membershipLogicV2.options.address)
        })
    })

    it("State wallet address can be changed by owner only.", async () => {
        await expectAsyncThrow(async () => {
            await send(membershipState, CUSTOMER, "setWallet", CUSTOMER_TWO)
        })
    })

    context("Upgrade-ability: Owner of state contract can change from version_n logic contract to version_n+1 logic contract and the states are still persisted.",
        async () => {
            let membershipWallet
            let beforeWalletBalance
            let membershipCount
            let afterWalletBalance

            beforeEach("OAX Token Sale contract owner marks operator and customer kyc verified;" +
                "and transfers customer enough oax token;" +
                "customer call logic contract to successfully add participant member;" +
                "logic contract change state contract owner from itself to logic contract version two.",
                async () => {
                    await send(memberships, DEPLOYER, "setOperator", OPERATOR)
                    await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, PARTICIPANT_MEMBERSHIP_FEE)
                    await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, VOTING_MEMBERSHIP_FEE)
                    await send(oaxToken, DEPLOYER, "kycVerify", OPERATOR)
                    await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER)

                    await send(oaxToken, DEPLOYER, "finalise")
                    await send(oaxToken, DEPLOYER, "transfer", CUSTOMER, VOTING_MEMBERSHIP_FEE)

                    membershipWallet = await call(membershipState, "wallet")
                    beforeWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

                    await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
                    await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)

                    membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                    afterWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

                    expect(membershipCount > 0).eql(true)
                    expect(membershipCount).eq(1)
                    expect(afterWalletBalance - beforeWalletBalance).eq(PARTICIPANT_MEMBERSHIP_FEE)

                    //set state contract owner to be logic contract
                    await send(memberships, DEPLOYER, "changeStateOwner", membershipLogicV2.options.address)
                })

            it("State persists when membership logic change from version one to version two;",
                async () => {
                    //states are the same
                    membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                    expect(membershipCount > 0).eql(true)
                    expect(membershipCount).eq(1)
                })

            it("and states can be updated by the version two contract after the upgrade;",
                async () => {
                    //states can be updated
                    await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
                    await send(membershipLogicV2, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)

                    membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                    afterWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

                    expect(membershipCount > 0).eql(true)
                    expect(membershipCount).eq(2)
                    expect(afterWalletBalance - beforeWalletBalance).eq(2 * PARTICIPANT_MEMBERSHIP_FEE)
                })

            it("but the states can no longer be updated by version one contract.",
                async () => {
                    //"version one logic contract can't update states any more.",
                    await expectAsyncThrow(async () => {
                        await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
                        await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
                    })
                })
        })

})