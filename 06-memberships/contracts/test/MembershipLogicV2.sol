pragma solidity ^0.4.19;


import "../MembershipState.sol";
import "../Ownable.sol";


contract MembershipLogicV2 is Ownable {
    MembershipState public membershipState;

    address dummyChange;

    function MembershipLogicV2(MembershipState membershipState_)
    Ownable()
    public
    {
        membershipState = membershipState_;
        setSuperAdmin(msg.sender);
    }

    function addMember(uint8 membershipType)
    public
    returns (bool)
    {
        return addMemberFor(membershipType, msg.sender);
    }

    function removeMember(uint8 membershipType)
    public
    returns (bool)
    {
        return removeMemberFor(membershipType, msg.sender);
    }

    function transferMember(uint8 membershipType, address to)
    public
    {
        transferMemberFor(membershipType, msg.sender, to);
    }

    function addMemberFor(uint8 membershipType, address member)
    public
    returns (bool)
    {
        return membershipState.addMemberFor(membershipType, msg.sender, member);
    }

    function removeMemberFor(uint8 membershipType, address member)
    public
    returns (bool)
    {
        return membershipState.removeMemberFor(membershipType, msg.sender, member);
    }

    function transferMemberFor(uint8 membershipType, address from, address to)
    public
    {
        require(
        (msg.sender == from) ||
        isAuthorisedToRequestTransfer(msg.sender)
        );
        membershipState.transferMemberFor(membershipType, from, to);
    }

    function isAuthorisedToRequestTransfer(address requester)
    public
    view
    returns (bool){
        return (superAdmin[requester] == true) || (operator[requester] == true);
    }

    mapping (address => bool) public superAdmin;

    mapping (address => bool) public operator;

    function setSuperAdmin(address address_)
    public
    {
        require(
        (msg.sender == owner) ||
        (superAdmin[msg.sender] == true)
        );
        superAdmin[address_] = true;
    }

    function revokeSuperAdmin(address address_)
    public
    {
        require(address_ != msg.sender);
        require(
        (msg.sender == owner) ||
        (superAdmin[msg.sender] == true)
        );
        superAdmin[address_] = false;
    }

    function setOperator(address address_)
    public
    {
        require(
        (msg.sender == owner) ||
        (superAdmin[msg.sender] == true)
        );
        operator[address_] = true;
    }

    function revokeOperator(address address_)
    public
    {
        require(
        (msg.sender == owner) ||
        (superAdmin[msg.sender] == true)
        );
        operator[address_] = false;
    }

    function addMembershipType(uint8 membershipType, uint8 price)
    public
    {
        membershipState.addMembershipType(membershipType, price);
    }

    function changeStateOwner(address newOwner)
    public
    {
        membershipState.setOwner(newOwner);
    }
}