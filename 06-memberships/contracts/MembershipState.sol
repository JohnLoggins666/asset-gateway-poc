pragma solidity ^0.4.19;


import "Ownable.sol";


contract MembershipState is Ownable {
    using SafeMath for uint;

    //if need oax specific features in future,
    //it can be factored into the logic contract
    //that's controlling this state contract
    ERC20Interface public oaxToken;

    address public wallet;

    mapping (uint8 => uint) public membershipPrice;

    //keeps a counter of how many same membership 'ticket' one address has
    mapping (uint8 => mapping (address => uint)) public members;

    mapping (uint8 => mapping (address => bool)) public approved;

    function MembershipState(ERC20Interface _oaxToken, address _wallet)
    Ownable()
    public
    {
        require(address(_oaxToken) != address(0));
        require(_wallet != address(0));
        oaxToken = _oaxToken;
        wallet = _wallet;
    }

    function addMembershipType(uint8 membershipType, uint price)
    public
    onlyIfOwner
    {
        require(membershipType > 0);
        require(price > 0);
        membershipPrice[membershipType] = price;
    }

    function addMemberFor(uint8 membershipType, address requestedBy, address member)
    public
    onlyIfOwner
    returns (bool)
    {
        require(membershipType > 0);
        require(requestedBy != address(0));
        require(member != address(0));
        require(membershipPrice[membershipType] > 0);
        require(oaxToken.allowance(requestedBy, address(this)) >= membershipPrice[membershipType]);
        //for now do not burn in this state contract, there's still membership discussion going on around membership quorum;
        //for now just transfer to a wallet and handle it later;
        //the wallet could well be a contract address which does nothing but burning OAX.
        require(oaxToken.transferFrom(requestedBy, wallet, membershipPrice[membershipType]));
        members[membershipType][member] = members[membershipType][member].add(1);
        return members[membershipType][member] > 0;
    }

    function removeMemberFor(uint8 membershipType, address /*requestedBy*/, address member)
    public
    onlyIfOwner
    returns (bool)
    {
        require(membershipType > 0);
        require(member != address(0));
        require(membershipPrice[membershipType] > 0);
        //otherwise would overflow
        require(members[membershipType][member] > 0);
        members[membershipType][member] = members[membershipType][member].sub(1);
        return members[membershipType][member] > 0;
    }

    function transferMemberFor(uint8 membershipType, address from, address to)
    public
    onlyIfOwner
    {
        require(membershipType > 0);
        require(from != address(0));
        require(to != address(0));
        require(membershipPrice[membershipType] > 0);
        require(members[membershipType][from] > 0);
        members[membershipType][from] = members[membershipType][from].sub(1);
        members[membershipType][to] = members[membershipType][to].add(1);
    }

    function oaxAllowance(address _owner, address _spender)
    public
    view
    returns (uint)
    {
        return oaxToken.allowance(_owner, _spender);
    }

    function approveMembership(address address_, uint8 membershipType)
    public
    onlyIfOwner
    {
        require(address_ != address(0));
        require(membershipType > 0);
        require(membershipPrice[membershipType] > 0);
        approved[membershipType][address_] = true;
    }

    function disapproveMembership(address address_, uint8 membershipType)
    public
    onlyIfOwner
    {
        require(address_ != address(0));
        require(membershipType > 0);
        require(membershipPrice[membershipType] > 0);
        approved[membershipType][address_] = false;
    }

    //used to move accidentally received ERC20 tokens to somewhere manage-able
    function moveAllToken(ERC20Interface token, address to)
    public
    onlyIfOwner
    returns (bool)
    {
        require(address(token) != address(0));
        require(to != address(0));

        uint tokenBalance = token.balanceOf(address(this));
        if (tokenBalance > 0) {
            return token.transfer(to, tokenBalance);
        }
        else {
            return false;
        }
    }

    event LogSetWallet (address indexed owner);

    function setWallet(address wallet_)
    public
    onlyIfOwner
    {
        require(wallet_ != address(0));
        wallet = wallet_;
        LogSetWallet(wallet);
    }
}


contract ERC20Interface {
    uint public totalSupply;

    function balanceOf(address _owner) constant public returns (uint balance);

    function transfer(address _to, uint _value) public returns (bool success);

    function transferFrom(address _from, address _to, uint _value)
    public returns (bool success);

    function approve(address _spender, uint _value) public returns (bool success);

    function allowance(address _owner, address _spender) constant public
    returns (uint remaining);

    event Transfer(address indexed _from, address indexed _to, uint _value);

    event Approval(address indexed _owner, address indexed _spender,
    uint _value);
}


library SafeMath {

    // ------------------------------------------------------------------------
    // Add a number to another number, checking for overflows
    // ------------------------------------------------------------------------
    function add(uint a, uint b) internal pure returns (uint) {
        uint c = a + b;
        assert(c >= a && c >= b);
        return c;
    }

    // ------------------------------------------------------------------------
    // Subtract a number from another number, checking for underflows
    // ------------------------------------------------------------------------
    function sub(uint a, uint b) internal pure returns (uint) {
        assert(b <= a);
        return a - b;
    }
}