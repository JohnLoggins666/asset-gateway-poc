const {
    address,
    wad,
    sig,
    send,
    call,
    create
} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER, OPERATOR, WALLET, LIMIT = wad(10000)) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        OpenANXToken,
        MembershipState,
        MembershipLogic,
        MembershipLogicV2,
        DSTokenBase
    } = contractRegistry

    const oaxToken = await deploy(OpenANXToken, OPERATOR)
    const membershipState = await deploy(MembershipState, oaxToken.options.address, WALLET)
    const memberships = await deploy(MembershipLogic, membershipState.options.address)
    const membershipLogicV2 = await deploy(MembershipLogicV2, membershipState.options.address)
    // await send(membershipState, DEPLOYER, "setOwner", memberships.options.address)
    const randomErc20Token = await deploy(DSTokenBase, 100000000000)

    return {
        oaxToken,
        membershipState,
        memberships,
        membershipLogicV2,
        randomErc20Token
    }
}

module.exports = {
    base
}
